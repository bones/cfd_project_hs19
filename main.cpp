// main.cpp

#include <iostream>
#include <fstream>
#include "ThermoclineStorage.hpp"
#include "BoundaryConditions.hpp"

int main() {
    double V = 300;
    const double pi = std::atan(1) * 4;
    double diam = 4;
    double height = 4*V / (pi * diam*diam);

    // Read parameters
    SimConfig cfg = SimConfig("domain.in", "parameters.in");
    cfg.domain_diam = diam;
    cfg.domain_len = height;
    cfg.recalculate();

    // Left and right boundary conditions
    ConstantBC bcl(873);
    ConstantBC bcr(293);
    cfg.setBCs(&bcl, &bcr);

    // Enforce CFL condition
    cfg.applyCFLTimestep();
    cfg.checkCFL(true);

    // Initialize and run the simulation
    ThermoclineStorage sim(cfg);
    sim.simulate();

    // Output final temperatures to file
    std::string fname = "out/main.out";
    std::ofstream os;
    os.open(fname);
    sim.writeTemperatures(os, true);
    os.close();
}

