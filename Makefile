# Makefile

.PHONY: all
all: build plots

.PHONY: build
build: build/Makefile
	cd build && make all

.PHONY: plots
plots: plot/.make_marker
	python3 allplot.py out/ plot/

.PHONY: ovsplots
ovsplots:
	mkdir -p plot/ovs/
	python3 ovsplot.py out/ovs/ plot/ovs/ "OVS"

.PHONY: main
main: build/Makefile out/.make_marker plot/.make_marker
	mkdir -p log
	mkdir -p out/ovs/
	cd build && make main
	./build/main | tee log/main.log
	#python3 logplot.py out/main.out plot/main.pdf
	python3 plot.py out/main.out plot/main.pdf

.PHONY: ovs
ovs: build out/.make_marker plot/.make_marker
	mkdir -p log
	mkdir -p out/ovs/
	./build/ovs | tee log/ovs.log
	make ovsplots
	make plots

.PHONY: pt5
pt5: build
	mkdir -p log
	mkdir -p out/pt5/
	./build/pt5 | tee log/pt5.log
	mkdir -p plot/pt5/
	python3 pt5_plot.py out/pt5/ plot/pt5/

.PHONY: pt6
pt6: build
	mkdir -p log
	mkdir -p out/pt6/
	./build/pt6 | tee log/pt6.log
	mkdir -p plot/pt6/
	python3 allplot.py out/pt6/ plot/pt6/

plot/initial_temperature.pdf: out/initial_temperature.out
	python3 plot.py $< $@ "Initial temperature" "Temperature [K]"

plot/states.pdf: out/states.out
	python3 plot.py $< $@ "States" "State no. [-]"

out/initial_temperature.out: domain.in parameters.in build/Makefile out/.make_marker
	cd build && make all
	echo "$@" | ./build/write_initial_temps

out/states.out: domain.in parameters.in build/Makefile out/.make_marker
	cd build && make
	echo "$@" | ./build/write_states

build/Makefile: CMakeLists.txt cfdsim/CMakeLists.txt
	mkdir -p build
	cd build && cmake ..

# Used to make sure the plot/ directory exists
plot/.make_marker:
	mkdir -p plot/
	touch $@

# Used to make sure the out/ directory exists
out/.make_marker:
	mkdir -p out/
	touch $@


.PHONY: clean
clean:
	rm -rf build out plot

