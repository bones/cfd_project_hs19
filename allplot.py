# allplot.py
# Similar to plot.py, but automatically plots all output files in given
# directory

import sys
import matplotlib.pyplot as plt

from os import listdir
from os.path import isfile, join

OUTPUT_FORMAT = "png"
OUTPUT_DPI = 300

if len(sys.argv) < 3:
    print("Error: too few arguments!")
    print("Usage: ./allplot.py input_data_directory output_directory [plot_title]")
    exit()

# Read command line arguments
datadir = sys.argv[1]
outdir = sys.argv[2]

plottitle = ''
ylabel = "Temperature [K]"
if len(sys.argv) >= 4:
    plottitle = sys.argv[3] + " - "
    # if len(sys.argv) >= 5:
    #     ylabel = sys.argv[4]

datafiles = [
        f for f in listdir(datadir) if isfile(join(datadir,f))
    ]

# Read input data file
labels = []
data = []

for datafile in datafiles:
    print(f"\nProcessing file {join(datadir, datafile)}...")

    if datafile[-4:] == ".out":
        outfile = datafile[:-4]
    else:
        print("Skipping.")
        continue
    outfile = join(outdir, f"{outfile}.{OUTPUT_FORMAT}")

    with open(join(datadir, datafile)) as f:
        content = f.read().splitlines()
        # First row is assumed to contain labels for each column
        labels = [l.strip() for l in content[0].split('\t')]
    
        # Use the number of labels to determine the number of data columns
        data = [[] for _ in range(len(labels))]
        for line in content[1:]:
            i = 0
            for num in line.split():
                data[i].append(float(num))
                i += 1
    
    # Initialize plot
    fig, ax = plt.subplots()
    
    # Plot each data set, using first column as x-axis data
    for i in range(1, len(labels)):
        ax.plot(data[0], data[i], label=labels[i])
    
    # Plot's aesthetics
    ax.set_title(plottitle)
    ax.set_xlabel(labels[0])
    ax.set_ylabel(ylabel)
    ax.legend()
    
    # Save figure
    print(f"Saving to {outfile}")
    fig.savefig(outfile, dpi=OUTPUT_DPI)
    plt.close(fig)

