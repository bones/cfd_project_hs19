// write_initial_temps.cpp
// Initializes a simulation object and writes the initial temperatures to an output file

#include <iostream>
#include <fstream>
#include "ThermoclineStorage.hpp"

int main() {
    std::string outfile;
    std::cout << "\nOutput file name to write temperatures: ";
    std::cin >> outfile;
    std::cout << std::endl;

    // Initialize simulation
    SimConfig cfg = SimConfig("domain.in", "parameters.in");
    auto* bc = new ConstantBC(cfg.initial_temp);
    cfg.setBCs(bc, bc);

    ThermoclineStorage sim = ThermoclineStorage(cfg);

    std::ofstream os;
    os.open(outfile);
    sim.writeTemperatures(os);
    os.close();
    std::cout << "\n\n";

    delete bc;
}

