# cfd_project_hs19

Project for lecture "Fundamentals of CFD methods" HS19

## Dependencies

This project is written in C++11, with CMake (>= 3.1) as a build system.
Plotting is done with Python (>= 3.6) and requires the Matplotlib library.

## Building and running

The Makefile in the project root can be used to compile the project and generate plots.

`make build` will compile the project using CMake in the `build/` subdirectory.

`make plots` will run each of the programs and place the output files in the `out/` directory, then generate each of the plots in the `plots/` directory.

## Project structure

```
_cfd_project_hs19
|-- build/                      // Build directory
|-- cfdsim/                     // CFD code lives here
|   |-- ChargeState.hpp         // Class for charge state
|   |-- CMakeLists.txt          // For compiling cfdsim library
|   |-- DischargeState.hpp      // Class for discharge state
|   |-- IdleState.hpp           // Class for idle state
|   |-- SimulationDomain.hpp    // Struct representing the domain
|   |-- SimulationState.cpp
|   |-- SimulationState.hpp     // Base class for states
|   |-- ThermoclineStorage.cpp  
|   `-- ThermoclineStorage.hpp  // Main simulation class
|-- out/                        // Directory for output files
|-- plot/                       // Directory for plots
|-- CMakeLists.txt              // For compiling entire project
|-- compile_commands.json -> build/compile_commands.json    // Symlink for IDE (development)
|-- domain.in                   // Input file for domain configuration
|-- parameters.in               // Input file for physical parameters
|-- main.cpp                    // Used mainly for development
|-- Makefile                    // Makefile to produce plots and build project
|-- plot.py                     // For plotting results
|-- README.md                   // For explaining stuff
|-- write_initial_temps.cpp     // Initializes simulation and writes initial temperatures to output file
`-- write_states.cpp            // Initializes simulation and writes states over time to output file
```

### Plotting script

The file `plot.py` is a generic plotting script used to generate graphs. Documentation is available [here](http://zumguy.com/automatic-plotting-with-python/).

### `write_initial_temps` and `write_states`

The programs `write_initial_temps` and `write_states` are used to generate the plots for the first two exercises. They read the filename to output to from standard input.
To run them using `bash` for example:

```bash
echo "out/states.out" | ./build/write_states
```

This is usually unnecessary since `make plots` also runs both programs.


## domain.in

This file contains the configuration of the simulation domain (time and space) and discretization.
```c++
10      // Domain height [m]
2       // Domain diameter [m]
50      // Number of cells for computation [-]

10800   // Duration of charging state [s]
32400   // Duration of first idle state [s]
21600   // Duration of discharge state [s]
21600   // Duration of second idle state [s]

1       // Number of cycles to simulate [-]
1       // Timestep size [s]

1e-4    // Tolerance on relative error (used for steady-state/exergy simulation) [-]
0       // Whether to perform a steady-state simulation [bool]
0       // Whether to use exergy mode (only used for transient simulation) [bool]
```

If the duration of one of the states is 0, it will be skipped.

## parameters.in

This file contains physical parameters for the simulation.

```c++
293     // Initial temperature [K]
293     // Inflow temperature during charging [K]
873     // Inflow temperature during discharging [K]
288.15  // Reference temperature (for exergy calculation) [K]

0.4     // epsilon
0.03    // ds

2600    // rhos
1835.6  // rhof

900.0   // Cs
1511.8  // Cpf

2.0     // ks
0.52    // kf

2.63    // muf
0.1     // mdotf
```
