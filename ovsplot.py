# ovsplot.py
# Similar to plot.py, but specifically to process
# the multiple outputs of ovs.cpp

import sys
import matplotlib.pyplot as plt
from numpy import log

from os import listdir
from os.path import isfile, join

OUTPUT_FORMAT = "png"
OUTPUT_DPI = 200
MARKERS = ['o', '^', 'o', '^', 's', 's', 's']
COLOURS = ['blue', 'xkcd:azure', 'red', 'orange']

if len(sys.argv) < 3:
    print("Error: too few arguments!")
    print("Usage: ./ovsplot.py input_data_directory output_directory [plot_title]")
    exit()

# Read command line arguments
datadir = sys.argv[1]
outdir = sys.argv[2]

plottitle = ''
ylabel = ''
if len(sys.argv) >= 4:
    plottitle = sys.argv[3] + " - "
    # if len(sys.argv) >= 5:
    #     ylabel = sys.argv[4]

datafiles = [
        join(datadir, f) for f in listdir(datadir) if isfile(join(datadir,f))
    ]

for datafile in datafiles:
    print(f"\nProcessing {datafile}...")

    # Read input data file
    labels = []
    data = []
    
    with open(datafile) as f:
        content = f.read().splitlines()

        Pe = content[0]
        n = content[1]

        content = content[2:]

        # First row is assumed to contain labels for each column
        labels = [l.strip() for l in content[0].split('\t')]
    
        # Use the number of labels to determine the number of data columns
        data = [[] for _ in range(len(labels))]
        for line in content[1:]:
            i = 0
            for num in line.split():
                data[i].append(float(num))
                i += 1
    
    plottitle_tail = f"Pe = {Pe}, n = {n}"
    ylabel = f"log(E)"
    


    # Initialize error plot
    fig, ax = plt.subplots()

    outfile = join(outdir, f"ovs_Pe{Pe}_n{n}.{OUTPUT_FORMAT}")
    
    # Plot each data set, using first column as x-axis data
    for i in range(1, 5):
        ax.plot(data[0], data[i], label=labels[i], marker=MARKERS[i-1],
                color=COLOURS[i-1])
    
    # Plot's aesthetics
    ax.set_title(plottitle + plottitle_tail)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(f"log({labels[0]})")
    ax.set_ylabel(ylabel)
    ax.legend()
    
    # Save figure
    print(f"Saving to {outfile}")
    fig.savefig(outfile, dpi=OUTPUT_DPI)
    plt.close(fig)



    # Initialize slopes plot
    fig, ax = plt.subplots()

    outfile = join(outdir, f"ovsslopes_Pe{Pe}_n{n}.{OUTPUT_FORMAT}")

    # Calculate slopes
    slopes = [[] for _ in range(4)]
    for j in range(1, 5):
        for i in range(1, len(data[0])):
            slopes[j-1].append(
                      abs(log(data[j][i]) - log(data[j][i-1]))
                    / abs(log(data[0][i]) - log(data[0][i-1]))
                )
            # slopes[1].append(
            #           abs(log(data[2][i]) - log(data[2][i-1]))
            #         / abs(log(data[0][i]) - log(data[0][i-1]))
            #     )
    
    # Plot each data set, using first column as x-axis data
    for i in range(len(slopes)):
        ax.plot(data[0][:-1], slopes[i], label=labels[i+1], marker=MARKERS[i],
                color=COLOURS[i])

    ax.plot(data[0][:-1], [1 for _ in slopes[0]], '--g')
    ax.plot(data[0][:-1], [2 for _ in slopes[1]], '--g')
    
    # Plot's aesthetics
    ax.set_title(f"{plottitle}Order of accuracy - {plottitle_tail}")
    ax.set_xscale("log")
    # ax.set_yscale("log")
    ax.set_ylim(0, 4)
    ax.set_xlabel(f"log({labels[0]})")
    ax.set_ylabel("Order of accuracy p")
    ax.legend()
    
    # Save figure
    print(f"Saving to {outfile}")
    fig.savefig(outfile, dpi=OUTPUT_DPI)
    plt.close(fig)



    # Initialize timings plot
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    
    outfile = join(outdir, f"ovstimings_Pe{Pe}_n{n}.{OUTPUT_FORMAT}")

    # Plot each data set, using first column as x-axis data
    ln1 = ax1.plot(data[0], data[5], "-k", label=labels[5], marker="o")
    ln2 = ax2.plot(data[0], data[6], "-r", label=labels[6], marker="o")
    lns = ln1+ln2
    labs = [l.get_label() for l in lns]
    ax1.legend(lns, labs)
    
    # Plot's aesthetics
    ax1.set_title(plottitle + plottitle_tail)
    ax1.set_xscale("log")
    ax1.set_yscale("log")
    ax2.set_yscale("log")
    ax1.set_xlabel(f"log({labels[0]})")
    ax1.set_ylabel(f"log({labels[5]})")
    ax2.set_ylabel(f"log({labels[6]})")
    
    # Save figure
    print(f"Saving to {outfile}")
    fig.savefig(outfile, dpi=OUTPUT_DPI)
    plt.close(fig)

