// SimConfig.hpp

#ifndef SIMCONFIG_H
#define SIMCONFIG_H

#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <memory>

#include "ManufacturedSolutions.hpp"
#include "BoundaryConditions.hpp"

/**
 * A helper class for reading configuration files and performing basic checks on input values.
 */
struct SimConfig {
    /**
     * Whether or not to use MMS with this simulation.
     */
    bool isMMS = false;

    /**
     * Pointer to the manufactured solution for MMS.
     * Only required it isMMS == true
     */
    ManufacturedSolution* mms = nullptr;
    //std::unique_ptr<ManufacturedSolution> mms = nullptr;

    /**
     * Pointer to left boundary condition object.
     * Only required if charge_duration > 0.
     */
    BoundaryCondition* left_bc = nullptr;

    /**
     * Pointer to right boundary condition object.
     * Only required if discharge_duration > 0.
     */
    BoundaryCondition* right_bc = nullptr;

    double domain_len;
    double domain_diam;
    unsigned int ncells;

    double charge_duration;
    double idle1_duration;
    double discharge_duration;
    double idle2_duration;
    //unsigned int nsteps_per_cycle;
    unsigned int ncycles;

    double dt;
    double tol;
    bool steady_state;
    bool exergy_mode;

    double initial_temp;
    double Tc;
    double Td;
    double Tref;
    double alpha_f;
    double alpha_s;

    double epsilon;
    double ds;
    double rhos;
    double rhof;
    double Cs;
    double Cpf;
    double ks;
    double kf;
    double muf;
    double mdotf;

    double hvf;
    double hvs;

    double u_f_charging;
    double u_f_discharging;

    /**
     * Reads parameters from files.
     */
    SimConfig(std::string domainfile, std::string paramsfile);

    /**
     * Default constructor.
     */
    SimConfig();

    /**
     * Reads parameters from std::cin, writing prompts to std::cout
     */
    // SimConfig(bool);

    /**
     * Copy constructor.
     */
    SimConfig(const SimConfig& other);

    /**
     * Copy assignment operator.
     */
    //TODO
    //SimConfig& operator=(SimConfig other);

    /**
     * Destructor.
     */
    ~SimConfig();

    /**
     * Explicitly define boundary conditions.
     */
    void setBCs(BoundaryCondition* l, BoundaryCondition* r);

    /**
     * Define manufactured solution to be used.
     * is_mms will be switched to true and boundary conditions will be extracted from MMS.
     */
    void setMMS(ManufacturedSolution* mms);

    /**
     * Given a Peclet number, calculate fluid velocity and timestep to respect
     * the CFL conditions.
     */
    void applyPecletNumber(double Pe);

    /**
     * Set the timestep to the largest allowed by the CFL conditions.
     * @param fraction  Amount to scale the timestep by.
     */
    void applyCFLTimestep(double fraction = 0.95);

    /**
     * Check CFL conditions for current configuration.
     * If verbose is set to true, output will be logged to std::cout.
     */
    bool checkCFL(bool verbose = false);

    /**
     * Performs basic checks on input values and ranges and throws an exception if they are invalid.
     */
    void verify_values() const;

    /**
     * Recalculate certain values based on modified inputs.
     */
    void recalculate();

};

#endif
