// SimConfig.cpp

#include "SimConfig.hpp"

void SimConfig::setBCs(BoundaryCondition* l, BoundaryCondition* r) {
    if (l)
        left_bc = l->clone();
    if (r)
        right_bc = r->clone();
}

void SimConfig::setMMS(ManufacturedSolution* mms) {
    //std::cout << "[SimConfig::setMMS()]" << std::endl;
    if (this->mms)
        delete this->mms;
    this->mms = mms->clone();
    setBCs(mms->getLeftBC(), mms->getRightBC());
    isMMS = true;
}

void SimConfig::applyPecletNumber(double Pe) {
    if (isMMS)
        //std::cerr << "\n\n***Warning***\nModifying u after having set MMS solution! [SimConfig::applyPecletNumber]\n\n";
    u_f_charging = Pe * alpha_f / domain_len;
    u_f_discharging = -u_f_charging;

    applyCFLTimestep();
}

void SimConfig::applyCFLTimestep(double f) {
    double dx = domain_len / ncells;
    double dt1 = 2 * alpha_f / (u_f_charging * u_f_charging) + dx / u_f_charging;
    double dt2 = (dx*dx) / (u_f_charging * dx + 2*alpha_f);
    double dt3 = (dx*dx) / (2*alpha_s);
    dt = f * std::min(dt1, std::min(dt2, dt3));
}

bool SimConfig::checkCFL(bool verbose) {
    double dx = domain_len / ncells;
    // Note: assumes abs(u_f_discharging) <= u_f_charging and u_f_charging >= 0
    double s = u_f_charging * dt / dx;
    double df = alpha_f * dt / (dx*dx);
    double ds = alpha_s * dt / (dx*dx);

    if (verbose) {
        std::cout << "\n### Checking CFL conditions.\n" << std::endl;
        std::cout << "uf =\t" << u_f_charging << std::endl;
        std::cout << "dt =\t" << dt << std::endl;
        std::cout << "dx =\t" << dx << std::endl;
        std::cout << "alphaf=\t" << alpha_f << std::endl;
        std::cout << "alphas=\t" << alpha_s << std::endl;
        std::cout << std::endl;
        std::cout << "sigma =\t" << s << std::endl;
        std::cout << "df = \t" << df << std::endl;
        std::cout << "ds = \t" << ds << std::endl;
    }

    if (!(s*s <= s + 2*df)) {
       if (verbose)
           std::cout << "(s*s <= s + 2*df) failed.";
       return false;
    }

    if (!(s + 2*df <= 1)) {
       if (verbose)
           std::cout << "(s + 2*df <= 1) failed.";
       return false;
    }

    if (!(2 * ds <= 1)) {
       if (verbose)
           std::cout << "(2 * ds <= 1) failed.";
       return false;
    }

    if (verbose)
        std::cout << "\nCFL conditions satisfied." << std::endl;

    return true;
}

void SimConfig::verify_values() const {
    if (domain_len <= 0 || domain_diam <= 0)
        throw std::out_of_range("Domain length and diameter must be > 0");
    if (ncells == 0)
        throw std::out_of_range("Number of cells must be > 0");
    if (charge_duration < 0 || idle1_duration < 0 || discharge_duration < 0 || idle2_duration < 0)
        throw std::out_of_range("Durations must be >= 0");
    if (ncycles == 0) // nsteps_per_cycle == 0 || 
        throw std::out_of_range("Number of cycles must be > 0");
        //throw std::out_of_range("Number of steps per cycle and number of cycles must be > 0");
    if (initial_temp <= 0)
        throw std::out_of_range("Initial temperature must be > 0");
    if (alpha_f < 0 || alpha_s < 0)
        throw std::out_of_range("Diffusivities must be >= 0");
    if (dt <= 0 || tol <= 0)
        throw std::out_of_range("Timestep and tolerance must be > 0");

    // BCs
    if (charge_duration > 0 && left_bc == nullptr)
        throw std::logic_error("Left boundary condition must be set if charge_duration > 0");
    if (discharge_duration > 0 && right_bc == nullptr)
        throw std::logic_error("Right boundary condition must be set if discharge_duration > 0");
}

SimConfig::SimConfig(std::string domainfile, std::string paramsfile) {

    std::ifstream is(domainfile);
    is >> domain_len;
    is >> domain_diam;
    is >> ncells;

    is >> charge_duration;
    is >> idle1_duration;
    is >> discharge_duration;
    is >> idle2_duration;

    is >> ncycles;
    is >> dt;

    is >> tol;
    is >> steady_state;
    is >> exergy_mode;
    is.close();



    is = std::ifstream(paramsfile);
    is >> initial_temp;
    is >> Tc;
    is >> Td;
    is >> Tref;

    is >> epsilon;
    is >> ds;

    is >> rhos;
    is >> rhof;

    is >> Cs;
    is >> Cpf;

    is >> ks;
    is >> kf;

    is >> muf;
    is >> mdotf;

    is.close();

    recalculate();
}

void SimConfig::recalculate() {
    double r = domain_diam / 2;
    const double pi = std::atan(1) * 4;
    double A = pi * r * r;
    double uf = std::abs(mdotf / (epsilon * rhof * A ));
    u_f_charging = uf;
    u_f_discharging = -uf;

    double Re = epsilon * rhof * uf * ds / muf;
    double Pr = muf * Cpf / kf;
    double Nufs = 0.255 / epsilon * std::pow(Pr, 1./3.) * std::pow(Re, 2./3.);
    double hfs = Nufs * kf / ds;
    double h = 1. / (1/hfs + ds/(10*ks));
    double hv = 6 * (1 - epsilon) * h / ds;
    hvf = hv / (epsilon * rhof * Cpf);
    hvs = hv / ((1 - epsilon) * rhos * Cs);

    alpha_f = kf / (epsilon * rhof * Cpf);
    alpha_s = ks / ((1-epsilon) * rhos * Cs);
}

SimConfig::SimConfig() {

}

/*
SimConfig::SimConfig(bool) {
    // Reads parameters from input stream
    std::cout << "\n(SimConfig) Reading simulation parameters from input stream.\n";

    std::cout << "\nInput domain length: ";
    std::cin >> domain_len;
    std::cout << "\nInput domain diameter: ";
    std::cin >> domain_diam;
    std::cout << "\nInput number of cells: ";
    std::cin >> ncells;

    std::cout << "\nInput duration of charging state: ";
    std::cin >> charge_duration;
    std::cout << "\nInput duration of first idle state: ";
    std::cin >> idle1_duration;
    std::cout << "\nInput duration of discharging state: ";
    std::cin >> discharge_duration;
    std::cout << "\nInput duration of second idle state: ";
    std::cin >> idle2_duration;
    
    //std::cout << "\nInput number of timesteps per cycle: ";
    //std::cin >> nsteps_per_cycle;
    std::cout << "\nInput number of cycles to simulate: ";
    std::cin >> ncycles;

    std::cout << "\nInput timestep duration: ";
    std::cin >> dt;
    std::cout << "\nInput tolerance: ";
    std::cin >> tol;
    std::cout << "\nRun steady state? [0/1]";
    std::cin >> steady_state;

    std::cout << "\nInput initial temperature: ";
    std::cin >> initial_temp;

    std::cout << "\nInput fluid diffusivity alpha_f: ";
    std::cin >> alpha_f;
    std::cout << "\nInput solid diffusivity alpha_s: ";
    std::cin >> alpha_s;

    std::cout << "\nInput fluid velocity u_f in charging state: ";
    std::cin >> u_f_charging;
    std::cout << "\nInput fluid velocity u_f in discharging state: ";
    std::cin >> u_f_discharging;

    verify_values();
}
*/

SimConfig::SimConfig(const SimConfig& other) {
    //std::cout << "[SimConfig::SimConfig(const SimConfig&)]" << std::endl;
    if (other.mms)
        mms = other.mms->clone();
    if (other.left_bc)
        left_bc = other.left_bc->clone();
    if (other.right_bc)
        right_bc = other.right_bc->clone();
    
    isMMS = other.isMMS;
    domain_len = other.domain_len;
    domain_diam = other.domain_diam;
    ncells = other.ncells;

    charge_duration = other.charge_duration;
    idle1_duration = other.idle1_duration;
    discharge_duration = other.discharge_duration;
    idle2_duration = other.idle2_duration;
    ncycles = other.ncycles;

    dt = other.dt;
    tol = other.tol;
    steady_state = other.steady_state;
    exergy_mode = other.exergy_mode;

    initial_temp = other.initial_temp;
    Tc = other.Tc;
    Td = other.Td;
    Tref = other.Tref;
    alpha_f = other.alpha_f;
    alpha_s = other.alpha_s;

    epsilon = other.epsilon;
    ds = other.ds;
    rhos = other.rhos;
    rhof = other.rhof;
    Cs = other.Cs;
    Cpf = other.Cpf;
    ks = other.ks;
    kf = other.kf;
    muf = other.muf;
    mdotf = other.mdotf;

    hvf = other.hvf;
    hvs = other.hvs;

    u_f_charging = other.u_f_charging;
    u_f_discharging = other.u_f_discharging;
}

SimConfig::~SimConfig() {
    //std::cout << "[SimConfig::~SimConfig()]" << std::endl;
    if (mms)
        delete mms;
    if (left_bc)
        delete left_bc;
    if (right_bc)
        delete right_bc;
}

