// ManufacturedSolutions.hpp

#ifndef MANUFACTUREDSOLUTIONS_H
#define MANUFACTUREDSOLUTIONS_H

#include <cmath> // std::cos
#include <iostream>

#include "BoundaryConditions.hpp"
#include "InitialConditions.hpp"

/**
 * Abstract base class for manufactured solutions.
 */
class ManufacturedSolution {
protected:
    BoundaryCondition* left_bc_;
    BoundaryCondition* right_bc_;
    InitialCondition* ic_;

public:

    ManufacturedSolution() {
        left_bc_ = new ConstantBC(0);
        right_bc_ = new ConstantBC(0);
        ic_ = new ConstIC(0);
    }

    ManufacturedSolution(const ManufacturedSolution& other) {
        //std::cout << "[ManufacturedSolution(const ManufacturedSolution&)]" << std::endl;
        left_bc_ = other.left_bc_->clone();
        right_bc_ = other.right_bc_->clone();
        ic_ = other.ic_->clone();
    }

    virtual ManufacturedSolution* clone() const = 0;

    virtual ~ManufacturedSolution() {
        //std::cout << "[ManufacturedSolution::~ManufacturedSolution]" << std::endl;
        delete left_bc_;
        delete right_bc_;
        delete ic_;
    }

    /**
     * Return value of the source term integrated
     * over cell [a; b] at time t.
     * If isFluid is true, the source term for the fluid phase is
     * evaluated. Otherwise, the solid phase is evaluated.
     */
    virtual double evalSourceTerm(
            double a, double b, double t, bool isFluid = true
        ) const = 0;

    /**
     * Return value of exact solution averaged
     * over cell [a; b] at time t.
     */
    virtual double evalAvgExactSolution(
            double a, double b, double t, bool isFluid = true
        ) const = 0;

    /**
     * Return value of exact solution 
     * at position x and time t.
     */
    virtual double evalExactSolution(
            double x, double t, bool isFluid = true
        ) const = 0;

    /**
     * Getter for left boundary condition.
     */
    BoundaryCondition* getLeftBC() const {
        return left_bc_;
    }

    /**
     * Getter for right boundary condition.
     */
    BoundaryCondition* getRightBC() const {
        return right_bc_;
    }

    /**
     * Getter for initial condition.
     */
    InitialCondition* get_ic() const {
        return ic_;
    }
};

/**
 * Helper that implements the clone() method on derived classes.
 * Ref: https://stackoverflow.com/questions/58708080/copy-constructing-data-members-of-abstract-type
 */
template <class Deriv>
class MMSHelper : public ManufacturedSolution {
public:
    virtual ManufacturedSolution* clone() const {
        // Defer to copy-constructor
        return new Deriv(static_cast<const Deriv&>(*this));
    }
};


/**
 * Manufactured solution: cos(kx)
 */
class CoskxMMS : public MMSHelper<CoskxMMS> {
    double kf_;
    double ks_;
    double u_f_;
    double alpha_f_;
    double alpha_s_;
    double hvf_;
    double hvs_;

public:
    CoskxMMS(double kf, double ks, double u_f, double alpha_f, double alpha_s, 
             double hvf, double hvs, double L)
        : kf_(kf), ks_(ks), u_f_(u_f), alpha_f_(alpha_f), alpha_s_(alpha_s),
          hvf_(hvf), hvs_(hvs)
    {
        left_bc_ = new ConstantBC(1);
        right_bc_ = new ConstantBC(std::cos(kf_ * L));
        ic_ = new CoskxIC(kf_);
    }

// pragmas to suppress warning: unused parameter t
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    double evalSourceTerm(double a, double b, double t, bool isFluid) const {
        //std::cout << "[CoskxMMS::evalSourceTerm] " << a << " " << b << " " << t << std::endl;
        if (isFluid) {
            return  u_f_       * (std::cos(kf_*b) - std::cos(kf_*a))
                    +alpha_f_*kf_ * (std::sin(kf_*b) - std::sin(kf_*a))
                    +hvf_/kf_*(std::sin(kf_*b) - std::sin(kf_*a))
                    -hvf_/ks_*(std::sin(ks_*b) - std::sin(ks_*a));
        } else {
            return  alpha_s_*ks_ * (std::sin(ks_*b) - std::sin(ks_*a))
                    -hvs_/kf_*(std::sin(kf_*b) - std::sin(kf_*a))
                    +hvs_/ks_*(std::sin(ks_*b) - std::sin(ks_*a));
        }
    }
    
    double evalAvgExactSolution(double a, double b, double t, bool isFluid) const {
        if (isFluid)
            return (std::sin(kf_*b) - std::sin(kf_*a)) / (kf_*(b-a));
        else
            return (std::sin(ks_*b) - std::sin(ks_*a)) / (ks_*(b-a));
    }
    
    double evalExactSolution(double x, double t, bool isFluid) const {
        if (isFluid)
            return std::cos(kf_ * x);
        else
            return std::cos(ks_ * x);
    }
#pragma GCC diagnostic pop
};

#endif
