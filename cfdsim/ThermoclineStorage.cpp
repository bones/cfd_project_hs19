// ThermoclineStorage.cpp

#include <iostream>
#include <sstream>
#include <iomanip>
#include <chrono>
#include "ThermoclineStorage.hpp"
#include "states/IdleState.hpp"
#include "states/ChargeState.hpp"
#include "states/DischargeState.hpp"

ThermoclineStorage::ThermoclineStorage()
    : params_(SimConfig()), domain_(params_)
{
    init();
}

ThermoclineStorage::ThermoclineStorage(const SimConfig& cfg)
    : params_(cfg), domain_(params_)
{
    init();
}

ThermoclineStorage::ThermoclineStorage(std::string domainfile, std::string paramsfile)
    : params_(SimConfig(domainfile, paramsfile)), domain_(params_)
{
    init();
}

void ThermoclineStorage::init() {
    params_.verify_values();

    num_cycles_ = params_.ncycles;
    dt_ = params_.dt;
    steady_state_ = params_.steady_state;
    tol_ = params_.tol;
    trackEnergy_ = params_.exergy_mode;

    //domain_ = SimulationDomain(params_);

    // Set up states
    states_.resize(0);

    if (params_.charge_duration > 0)
        states_.push_back(new ChargeState(this, params_.charge_duration, dt_, params_.u_f_charging, params_.left_bc, trackEnergy_));

    if (params_.idle1_duration > 0)
        states_.push_back(new IdleState(this, params_.idle1_duration, dt_));

    if (params_.discharge_duration > 0)
        states_.push_back(new DischargeState(this, params_.discharge_duration, dt_, params_.u_f_discharging, params_.right_bc, trackEnergy_));

    if (params_.idle2_duration > 0)
        states_.push_back(new IdleState(this, params_.idle2_duration, dt_));

    timesteps_per_cycle_ = 0;
    for (const auto& s : states_)
        timesteps_per_cycle_ += s->getNTimeSteps();

    num_states_ = states_.size();
}

ThermoclineStorage::~ThermoclineStorage() {
    for (unsigned i = 0; i < num_states_; i++) {
        delete states_[i];
    }
}


void ThermoclineStorage::printSummary() {
    //std::cout << "\n######################\n" << std::endl;
    std::cout << "\n### ThermoclineStorage configuration\n\n";
    std::cout << "L = \t\t" << params_.domain_len << std::endl;
    std::cout << "ncells =\t" << params_.ncells << std::endl;
    std::cout << "dx = \t\t" << params_.domain_len / params_.ncells << std::endl;
    std::cout << "dt = \t\t" << params_.dt << std::endl;
    std::cout << "u_f_charging = \t" << params_.u_f_charging << std::endl;
    std::cout << "MMS = \t\t" << params_.isMMS << std::endl;
    std::cout << "Steady-state = \t" << steady_state_ << std::endl;
    //std::cout << "\n######################\n" << std::endl;
}


void ThermoclineStorage::writeTemperatures(std::ostream& os, bool stepwise) {
    //os << "Position [m]\tT (fluid)  \tT (solid)\tT (initial)\n";
    os << "Position [m]\tT (fluid)  \tT (solid)\n";
    
    // With more than 64 cells the steps are really small anyway
    if (stepwise && domain_.ncells > 64)
        stepwise = false;

    for (uint i = 0; i < domain_.ncells; i++) {
        if (stepwise) {
            os << std::scientific 
                << i*domain_.dx << "\t" 
                << domain_.fluid_temp[i] << "\t" 
                << domain_.solid_temp[i] << "\t"
                //<< domain_.initial_temp[i] << "\t"
                << "\n";
            os << std::scientific 
                << (i+1)*domain_.dx << "\t" 
                << domain_.fluid_temp[i] << "\t" 
                << domain_.solid_temp[i] << "\t" 
                //<< domain_.initial_temp[i] << "\t"
                << "\n";
        } else {
            os << std::scientific 
                << i*domain_.dx << "\t" 
                << domain_.fluid_temp[i] << "\t" 
                << domain_.solid_temp[i] << "\t"
                //<< domain_.initial_temp[i] << "\t"
                << "\n";
        }
    }
}

void ThermoclineStorage::writeStates(std::ostream& os) {
    os << "Time [s]\tState no.\n";

    //std::cout << "ThermoclineStorage::writeStates() - " << timesteps_per_cycle_ << " " << num_cycles_ << "\n";
    for (uint i = 0; i < timesteps_per_cycle_ * num_cycles_; i++) {
        os << global_time_ << "\t" << getCurState().getStateType() << "\n";
        skip();
    }
}

void ThermoclineStorage::skip(unsigned int nsteps) {
    unsigned int i = 0;
    while (i < nsteps) {
        i += getCurState().skip(nsteps - i);
    }
    global_time_ += i*dt_;
}

void ThermoclineStorage::step(unsigned int nsteps) {
    if (!steady_state_)
        std::cout << "Advancing by " << nsteps << " timestep(s)." << std::endl;

    unsigned int i = 0;
    while (i < nsteps) {
        double prog = getCurState()
            .step(nsteps - i, steady_state_, params_.isMMS, params_.mms);
        stepcount_ += prog;
        i += prog;
    }
}

void ThermoclineStorage::simulate() {
    auto t1 = std::chrono::high_resolution_clock::now();
    if (!steady_state_) {
        std::cout << "\n### Running transient simulation\n" << std::endl;

        if (!trackEnergy_) { // Regular transient sim
            for (unsigned int c = 0; c < num_cycles_; c++) {
                std::cout << "\n\n## Starting cycle " << c+1 << std::endl;
                step(timesteps_per_cycle_);
                std::cout << std::endl;
            }
        } else { // Exergy-terminated transient sim
            double c = 1;
            do {
                std::cout << "\n\n## Starting cycle " << c++ << std::endl;
                step(timesteps_per_cycle_);
                std::cout << std::endl;

            } while (!exergy_termination_());

            // Compute energy capacity factor
            double Qc = states_[0]->getQ();
            double Qd = states_[2]->getQ();
            double Qstored = Qd - Qc;
            std::cout << "Stored energy: " << Qstored << std::endl;

            double Tc = params_.Tc;
            double Td = params_.Td;
            double capacity_factor = Qstored / domain_.computeMaxThermalEnergy(Tc, Td);
            std::cout << "Capacity factor: " << capacity_factor << "\n" << std::endl;

            std::cout << "Delta T: " << getCurState().getTemperatureChange().second << std::endl;
        }
    } else { // Steady-state simulation
        //std::cout << "\n### Running steady-state simulation\n" << std::endl;
        do {
            step(1);
        } while (!steady_state_termination_());
    }
    auto t2 = std::chrono::high_resolution_clock::now();

    auto d = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 )
             .count();

    /*
    std::ostringstream oss;
    oss << std::setfill('0')          // set field fill character to '0'
        << (d % 1000000000) / 1000000 // format seconds
        << "."
        << std::setw(3)               // set width of milliseconds field
        << (d % 1000000) / 1000       // format milliseconds
        << ""
        << std::setw(3)               // set width of microseconds field
        << d % 1000                   // format microseconds
        << " s";
    auto duration(oss.str());
    */

    runtime_ = (double) d / 1000000.0;

    //std::cout << "Wall-clock time: " << runtime_ << "s." << std::endl;
}

double ThermoclineStorage::l1Error_f() {
    if (!params_.isMMS)
        throw std::logic_error("Evaluation of error norm requires exact solution to be provided as ManufacturedSolution object.");

    double err = 0;
    double dx = domain_.length / domain_.ncells;
    for (unsigned int i = 0; i < domain_.ncells; i++) {
        //double exact = params_.mms->evalAvgExactSolution(
        //        i*dx, (i+1)*dx, global_time_
        //    );
        double exact = params_.mms->evalExactSolution(
                (i+.5)*dx, global_time_
            );
        err += std::abs(domain_.fluid_temp[i] - exact);
    }

    return err / domain_.ncells;
}

double ThermoclineStorage::lInfError_f() {
    if (!params_.isMMS)
        throw std::logic_error("Evaluation of error norm requires exact solution to be provided as ManufacturedSolution object.");

    double maxerr = 0;
    double dx = domain_.length / domain_.ncells;
    for (unsigned int i = 0; i < domain_.ncells; i++) {
        double exact = params_.mms->evalExactSolution(
                (i+.5)*dx, global_time_
            );
        double err = std::abs(domain_.fluid_temp[i] - exact);
        if (err > maxerr)
            maxerr = err;
    }

    return maxerr;
}

double ThermoclineStorage::l1Error_s() {
    if (!params_.isMMS)
        throw std::logic_error("Evaluation of error norm requires exact solution to be provided as ManufacturedSolution object.");

    double err = 0;
    double dx = domain_.length / domain_.ncells;
    for (unsigned int i = 0; i < domain_.ncells; i++) {
        //double exact = params_.mms->evalAvgExactSolution(
        //        i*dx, (i+1)*dx, global_time_
        //    );
        double exact = params_.mms->evalExactSolution(
                (i+.5)*dx, global_time_, false
            );
        err += std::abs(domain_.solid_temp[i] - exact);
    }

    return err / domain_.ncells;
}

double ThermoclineStorage::lInfError_s() {
    if (!params_.isMMS)
        throw std::logic_error("Evaluation of error norm requires exact solution to be provided as ManufacturedSolution object.");

    double maxerr = 0;
    double dx = domain_.length / domain_.ncells;
    for (unsigned int i = 0; i < domain_.ncells; i++) {
        double exact = params_.mms->evalExactSolution(
                (i+.5)*dx, global_time_, false
            );
        double err = std::abs(domain_.solid_temp[i] - exact);
        if (err > maxerr)
            maxerr = err;
    }

    return maxerr;
}

bool ThermoclineStorage::steady_state_termination_() {
    if (!steady_state_)
        throw std::logic_error("Cannot evaluate steady state termination condition in transient solver!");
    
    double diff = 0;
    double diff_s = 0;
    for (unsigned int i = 0; i < domain_.ncells; i++) {
        diff += std::abs(domain_.fluid_temp[i] - domain_.tmp_fluid_temp[i]);
        diff_s += std::abs(domain_.solid_temp[i] - domain_.tmp_solid_temp[i]);
    }
    diff /= domain_.ncells;
    diff_s /= domain_.ncells;
    diff = std::max(diff, diff_s);
    
    if (diff > 1e10) {
        //std::cout << "L1 norm residual: " << diff<< " (tolerance: " << tol_ << ")" << std::endl;
        //std::cout << "Detected divergence after " << stepcount_ << " steps. Terminating." << std::endl;
        return true;
    } else if (diff < tol_) {
        //std::cout << "L1 norm residual: " << diff<< " (tolerance: " << tol_ << ")" << std::endl;
        //std::cout << "Detected convergence after " << stepcount_ << " steps. Terminating." << std::endl;
        return true;
    } else {
        return false;
    }
}

bool ThermoclineStorage::exergy_termination_() { 
    // Compute exergy efficiency
    double Xi_ci, Xi_co, Xi_di, Xi_do;
    std::tie(Xi_ci, Xi_co) = states_[0]->getExergyFlux();
    std::tie(Xi_do, Xi_di) = states_[2]->getExergyFlux();

    double exergy_eff = (Xi_do - Xi_di) / (Xi_ci - Xi_co);
    
    std::cout << "Exergy efficiency: " << exergy_eff;

    double delta = std::abs(prev_exergy_eff_ - exergy_eff);
    std::cout << " (Residual: " << delta << ")" << std::endl;

    prev_exergy_eff_ = exergy_eff;

    return delta < tol_;
}

SimulationState& ThermoclineStorage::getCurState() {
    //std::cout << "ThermoclineStorage::getCurState() - " << cur_state_idx_ << "\t" << num_states_ << std::endl;
    //std::cout << states_[0]->getStateType();
    return *(states_[cur_state_idx_]);
}

void ThermoclineStorage::progress_state() {
    //std::cout << "ThermoclineStorage::progress_state()\n";
    getCurState().reset();
    cur_state_idx_++;
    if (cur_state_idx_ >= num_states_) {
        cur_state_idx_ = 0;
        cur_cycle_count_++;
        if (cur_cycle_count_ >= num_cycles_) {
            end_simulation();
        }
    }
}

unsigned int ThermoclineStorage::getStepCount() const {
    return stepcount_;
}

double ThermoclineStorage::getRuntime() const {
    return runtime_;
}

void ThermoclineStorage::end_simulation() {
    // TODO
    //std::cout << "ThermoclineStorage::end_simulation()\n";
}

