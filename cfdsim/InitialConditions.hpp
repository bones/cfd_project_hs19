// InitialConditions.hpp

#ifndef INITIAL_CONDITIONS
#define INITIAL_CONDITIONS

#include <cmath>

/**
 * Abstract base class for initial conditions.
 */
class InitialCondition {
public:
    /**
     * Return value of the fluid initial temperature
     * averaged over cell [a; b].
     */
    virtual double eval_f(double a, double b) = 0;

    /**
     * Return value of the solid initial temperature
     * averaged over cell [a; b].
     */
    virtual double eval_s(double a, double b) = 0;

    virtual ~InitialCondition() {

    }

    virtual InitialCondition* clone() const = 0;
};

/**
 * Helper that implements the clone() method on derived classes.
 * Ref: https://stackoverflow.com/questions/58708080/copy-constructing-data-members-of-abstract-type
 */
template <class Deriv>
class ICHelper : public InitialCondition {
public:
    virtual InitialCondition* clone() const {
        // Defer to copy-constructor
        return new Deriv(static_cast<const Deriv&>(*this));
    }
};

/**
 * Constant initial condition.
 */
class ConstIC : public ICHelper<ConstIC> {
    double v_;

public:
    ConstIC(double v)
        : v_(v)
    { }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    double eval_f(double a, double b) {
        return v_;
    }

    double eval_s(double a, double b) {
        return v_;
    }
#pragma GCC diagnostic pop
};

/**
 * Class representing initial condition cos(k*x),
 * used for the corresponding MMS.
 */
class CoskxIC : public ICHelper<CoskxIC> {
    double kf_;

public:
    CoskxIC(double k)
        : kf_(k)
    { }

    double eval_f(double a, double b) {
        //return (std::sin(kf_*b) - std::sin(kf_*a)) / (kf_*(b-a));
        return std::cos(kf_ * (a + b)*.5);
    }

    double eval_s(double a, double b) {
        return eval_f(a, b);
    }

};

#endif
