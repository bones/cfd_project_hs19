// SimulationDomain.hpp

#ifndef SIMDOMAIN_H
#define SIMDOMAIN_H
#include <vector>
#include "SimConfig.hpp"

/**
 * A struct representing the domain of the simulation.
 * Contains spatial dimensions, information about spatial discretization, 
 * and temperature values for solid and fluid phases.
 */
struct SimulationDomain {
    const SimConfig& cfg;

    double length;
    double diameter;
    unsigned int ncells;
    double dx;
    std::vector<double> fluid_temp;
    std::vector<double> tmp_fluid_temp;
    std::vector<double> solid_temp;
    std::vector<double> tmp_solid_temp;
    std::vector<double> initial_temp;

    SimulationDomain(const SimConfig& inputs)
        : cfg(inputs), length(cfg.domain_len), diameter(cfg.domain_diam), 
          ncells(cfg.ncells), dx(length/ncells)
    {
        double initial_temp = cfg.initial_temp;

        fluid_temp = std::vector<double>(ncells, initial_temp);
        tmp_fluid_temp = std::vector<double>(ncells, initial_temp);
        solid_temp = std::vector<double>(ncells, initial_temp);
        tmp_solid_temp = std::vector<double>(ncells, initial_temp);
        this->initial_temp = std::vector<double>(ncells, initial_temp);

        if (cfg.isMMS && cfg.mms->get_ic() != nullptr) {
            read_ICs(inputs.mms->get_ic());
        }
    }

    /**
     * Compute the integral of f over the domain at current time.
     * @param f Function taking the fluid and solid temperatures as argument
     */
    template<typename Function>
    double domain_integral(Function f) const {
        double F = 0;

        const auto& Tf = fluid_temp;
        const auto& Ts = solid_temp;

        // Integration via trapezoidal rule
        for (unsigned int i = 0; i < ncells - 1; i++) {
            F += .5 * dx * (f(Tf[i], Ts[i]) + f(Tf[i+1], Ts[i+1]));
        }

        return F;
    }

    /**
     * Compute thermal energy Q in the system at current time.
     */
    double computeThermalEnergy(double Td) const {
        const double pi = std::atan(1) * 4;
        const double d2 = diameter*diameter;
        const double epsilon = cfg.epsilon;
        const double rhof = cfg.rhof;
        const double Cpf = cfg.Cpf;
        const double rhos = cfg.rhos;
        const double Cs = cfg.Cs;

        auto ff = [Td](double Tf, double Ts) { return Tf - Td; };
        auto fs = [Td](double Tf, double Ts) { return Ts - Td; };

        double intf = domain_integral(ff);
        double ints = domain_integral(fs);

        return .25*pi*d2*(epsilon*rhof*Cpf * intf + (1-epsilon)*rhos*Cs * ints);
    }

    /**
     * Compute maximum theoretical thermal energy Qmax that could be stored in this
     * domain.
     */
    double computeMaxThermalEnergy(double Tc, double Td) const {
        const double pi = std::atan(1) * 4;
        const double d2 = diameter*diameter;
        const double epsilon = cfg.epsilon;
        const double rhof = cfg.rhof;
        const double Cpf = cfg.Cpf;
        const double rhos = cfg.rhos;
        const double Cs = cfg.Cs;

        return .25 * pi * d2 * length
                * (epsilon*rhof*Cpf + (1-epsilon)*rhos*Cs)
                * (Tc - Td);
    }

private:

    void read_ICs(InitialCondition* ic) {
        //std::cout << "SimulationDomain::read_ICs(.)" << std::endl;
        double dx = length / ncells;
        for (unsigned int i = 0; i < ncells; i++) {
            fluid_temp[i]     = ic->eval_f(i*dx, (i+1)*dx);
            tmp_fluid_temp[i] = ic->eval_f(i*dx, (i+1)*dx);
            solid_temp[i]     = ic->eval_s(i*dx, (i+1)*dx);
            tmp_solid_temp[i] = ic->eval_s(i*dx, (i+1)*dx);
            initial_temp[i]   = ic->eval_s(i*dx, (i+1)*dx);
        }
    }

};

#endif
