// ThermoclineStorage.hpp

#ifndef THERMOCLINE_H
#define THERMOCLINE_H

#include <iostream>
#include <limits>
#include "SimConfig.hpp"
#include "SimulationDomain.hpp"
#include "states/SimulationState.hpp"
#include "BoundaryConditions.hpp"

class ThermoclineStorage {
    friend class SimulationState;
    friend class ChargeState;
    friend class IdleState;
    friend class DischargeState;
private:
    SimConfig params_;
    SimulationDomain domain_;
    std::vector<SimulationState*> states_;

    unsigned int num_states_;
    unsigned int cur_state_idx_ = 0;
    unsigned int num_cycles_;
    unsigned int cur_cycle_count_ = 0;
    unsigned int timesteps_per_cycle_;
    double dt_;
    double tol_;
    bool steady_state_;
    double global_time_ = 0;
    unsigned int stepcount_ = 0;
    double runtime_ = 0;
    double prev_exergy_eff_ = std::numeric_limits<double>::infinity();

    // TODO make argument
    bool trackEnergy_ = false;

    //TODO rename private methods
    /**
     * Called by constructors, sets up internal data
     */
    void init();

    /**
     * Return a reference to the current state object
     */
    SimulationState& getCurState();

    /**
     * Proceed to the next state. Called by simulation states.
     * If we reached the end of a cycle, go back to the first state.
     * If we reached the specified number of cycles, end the simulation.
     */
    void progress_state();

    /**
     * End the simulation
     */
    void end_simulation();

    /**
     * Termination condition for steady state simulation.
     */
    bool steady_state_termination_();

    /**
     * Termination condition for exergy efficiency simulation.
     */
    bool exergy_termination_();

public:

    /**
     * Reads simulation parameters from SimConfig object
     */
    ThermoclineStorage(const SimConfig& cfg);

    /** 
     * Reads simulation parameters from configuration files
     */
    ThermoclineStorage(std::string domainfile, std::string paramsfile);
    
    /**
     * Reads simulation parameters from std::cin
     */
    ThermoclineStorage();

    ~ThermoclineStorage();

    /**
     * Print a summary of the parameters to std::cout
     */
    void printSummary();

    /**
     * Advance simulation clock by nsteps timesteps, without actually simulating
     */
    void skip(unsigned int nsteps = 1);

    /**
     * Advance simulation by nsteps timesteps
     */
    void step(unsigned int nsteps = 1);

    /**
     * Run the simulation, optionally with MMS
     */
    void simulate();

    /**
     * Returns the average L1 norm of the error over the cells (fluid phase).
     * Requires exact solution to be provided via MMS.
     */
    double l1Error_f();

    /**
     * Returns the L-infinity norm of the error over the cells (fluid phase).
     * Requires exact solution to be provided via MMS.
     */
    double lInfError_f();

    /**
     * Returns the average L1 norm of the error over the cells (solid phase).
     * Requires exact solution to be provided via MMS.
     */
    double l1Error_s();

    /**
     * Returns the L-infinity norm of the error over the cells (solid phase).
     * Requires exact solution to be provided via MMS.
     */
    double lInfError_s();

    /**
     * Write temperatures over space to output stream
     */
    void writeTemperatures(std::ostream& os, bool stepwise = false);

    /**
     * Write states over time to output stream
     */
    void writeStates(std::ostream& os);

    /**
     * Return the number of steps executed in the simulation.
     */
    unsigned int getStepCount() const;

    /**
     * Return the runtime of the last simulate() call.
     */
    double getRuntime() const;

};

#endif // THERMOCLINE_H
