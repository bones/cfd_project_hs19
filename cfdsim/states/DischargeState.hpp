// DischargeState.hpp

#ifndef DISCHARGESTATE_H
#define DISCHARGESTATE_H

#include "SimulationState.hpp"

/**
 * Implementation of SimulationState for discharging phase of the simulation.
 */
class DischargeState : public SimulationState {

    BoundaryCondition* right_bc_;

    void doBoundaries_(double dt, bool isMMS, ManufacturedSolution* mms);

    void step_(double dt, bool isMMS = false, ManufacturedSolution* mms = nullptr);

public:
    DischargeState(ThermoclineStorage* owner,
                   double duration,
                   double dt,
                   double u_f,
                   BoundaryCondition* r_bc,
                   bool trackEnergy = false)
    : SimulationState(owner, duration, dt, trackEnergy), right_bc_(r_bc)
    {
        if (u_f > 0)
            throw std::invalid_argument("u_f must be <= 0 for DischargeState");
        u_f_ = u_f;
    }

    ~DischargeState() {

    }

    unsigned int getStateType() const {
        return 2;
    }
};

#endif
