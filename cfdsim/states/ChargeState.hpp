// ChargeState.hpp

#ifndef CHARGESTATE_H
#define CHARGESTATE_H

#include "SimulationState.hpp"

/**
 * Implementation of SimulationState for charging phase of the simulation.
 */
class ChargeState : public SimulationState {

    BoundaryCondition* left_bc_;

    void doBoundaries_(double dt, bool isMMS, ManufacturedSolution* mms);

public:
    ChargeState(ThermoclineStorage* owner,
                double duration,
                double dt, 
                double u_f,
                BoundaryCondition* l_bc,
                bool trackEnergy = false)
    : SimulationState(owner, duration, dt, trackEnergy), left_bc_(l_bc)
    {
        if (u_f < 0)
            throw std::invalid_argument("u_f must be >= 0 for ChargeState");
        u_f_ = u_f;
    }

    ~ChargeState() {

    }

    unsigned int getStateType() const {
        return 1;
    }
};

#endif

