// IdleState.hpp

#ifndef IDLESTATE_H
#define IDLESTATE_H

#include "SimulationState.hpp"

/**
 * Implementation of SimulationState for idle phase of the simulation.
 */
class IdleState : public SimulationState {

    void doBoundaries_(double dt, bool isMMS, ManufacturedSolution* mms);

public:
    IdleState(ThermoclineStorage* owner, double duration, double dt)
    : SimulationState(owner, duration, dt)
    {
        u_f_ = 0;
    }

    ~IdleState() {

    }

    unsigned int getStateType() const {
        return 3;
    }
};

#endif
