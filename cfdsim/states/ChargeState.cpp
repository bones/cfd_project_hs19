// ChargeState.cpp

#include "ChargeState.hpp"
#include "ThermoclineStorage.hpp"

void ChargeState::doBoundaries_(double dt, bool isMMS, ManufacturedSolution* mms) {
    auto& domain = owner_->domain_;
    auto& params = owner_->params_;

    unsigned int N = domain.ncells;
    double dx = domain.dx;
    double L = domain.length;
    double af = params.alpha_f;
    double as = params.alpha_s;

    auto& Tf = domain.fluid_temp;
    auto& newTf = domain.tmp_fluid_temp;

    auto& Ts = domain.solid_temp;
    auto& newTs = domain.tmp_solid_temp;

    // Value at left boundary
    double T_bc = (*left_bc_)(0, cur_time_);

    newTf[0] = Tf[0] - u_f_*dt/dx*(Tf[0] - T_bc)
                   + af*dt/(dx*dx)*(Tf[1] - Tf[0]);
    newTf[N-1] = Tf[N-1] - u_f_*dt/dx*(Tf[N-1] - Tf[N-2])
                     + af*dt/(dx*dx)*(Tf[N-2] - Tf[N-1]);

    newTs[0]   = Ts[0]   + as*dt/(dx*dx)*(Ts[1]   - Ts[0]);
    newTs[N-1] = Ts[N-1] + as*dt/(dx*dx)*(Ts[N-2] - Ts[N-1]);

    if (isMMS) {
        newTf[0] += dt/dx * mms->evalSourceTerm(0, dx, cur_time_);
        newTf[N-1] += dt/dx * mms->evalSourceTerm(L-dx, L, cur_time_);
        newTs[0]   += dt/dx * mms->evalSourceTerm(0,   dx, cur_time_, false);
        newTs[N-1] += dt/dx * mms->evalSourceTerm(L-dx, L, cur_time_, false);
    }

    // Coupling term
    std::tie(newTf[0], newTs[0]) = 
        eval_coupling_(dt, newTf[0], newTs[0]);
    std::tie(newTf[N-1], newTs[N-1]) = 
        eval_coupling_(dt, newTf[N-1], newTs[N-1]);
}

