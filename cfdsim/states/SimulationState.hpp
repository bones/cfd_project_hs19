// SimulationState.hpp

#ifndef SIMSTATE_H
#define SIMSTATE_H

#include <iostream>
#include <utility>
#include "SimulationDomain.hpp"
class ThermoclineStorage;

/**
 * Abstract base class for simulation states.
 */
class SimulationState {
protected:
    ThermoclineStorage* owner_;
    double duration_;
    double dt_;
    bool trackEnergy_;
    double cur_time_ = 0;
    double u_f_ = 0; // Fluid velocity in this state

    // Internal energy at beginning of state, last time .step(...) was called
    double initial_stored_energy_;

    // Used for exergy calculation if trackEnergy_ == true
    std::vector<double> Tinlet_;
    std::vector<double> Toutlet_;
    std::vector<double> timesteps_;

    /**
     * Advances simulation by given timestep dt once.
     */
    virtual void step_(double dt, bool isMMS = false, ManufacturedSolution* mms = nullptr);

    /**
     * Perform a single timestep on the boundaries.
     * Implementation deferred to concrete child classes.
     */
    virtual void doBoundaries_(double dt, bool isMMS, ManufacturedSolution* mms) = 0;

    /**
     * Evaluate the effect of the coupling term on given Tf and Ts values.
     * @return Updated values for Tf and Ts respectively
     */
    std::pair<double, double>
        eval_coupling_(double dt, double Tf, double Ts);

private:

    /**
     * Log temperatures needed for exergy calculcation.
     */
    void logTemps_(double dt);

public:
    SimulationState(ThermoclineStorage* owner,
                    double duration,
                    double dt,
                    bool trackEnergy = false)
        : owner_(owner), duration_(duration), dt_(dt),
          trackEnergy_(trackEnergy)
    { }

    virtual ~SimulationState() {

    }

    virtual unsigned int getStateType() const = 0;

    /**
     * Returns the number of timesteps required to complete this state.
     * A call to state.step(state.getNTimeSteps()) will complete the state.
     */
    unsigned int getNTimeSteps() const;

    /**
     * Returns the internal energy which was evaluated at the beginning of the
     * last .step(.) call. Note that energy is evaluated only if 
     * trackEnergy_ == true
     */
    double getQ() const {
        return initial_stored_energy_;
    }

    /**
     * Returns exergy flux for the last cycle at inlet and outlet.
     */
    std::pair<double, double> getExergyFlux() const;

    /**
     * Return change in temperature at left and right boundaries over the last
     * cycle.
     */
    std::pair<double, double> getTemperatureChange() const;

    /** Advances time by the given timestep dt up to nsteps times.
     * Does NOT execute simulation.
     * If the state's duration is passed, stepping is interrupted
     *  and the owner is notified.
     * Returns the actual number of timesteps performed.
     */
    unsigned int skip(unsigned int nsteps = 1);

    /**
     * Advances simulation by given timestep dt up to nsteps times.
     * If the state's duration is passed, stepping is interrupted
     *  and the owner is notified.
     * Returns the actual number of timesteps performed.
     */
    unsigned int step(unsigned int nsteps = 1, bool steady_state = false, bool isMMS = false, ManufacturedSolution* mms = nullptr);

    /** 
     * Reset internal timer to 0
     */
    void reset();
};

#endif
