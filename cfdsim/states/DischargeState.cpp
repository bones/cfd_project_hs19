// DischargeState.cpp

#include "DischargeState.hpp"
#include "ThermoclineStorage.hpp"

void DischargeState::step_(double dt, bool isMMS, ManufacturedSolution* mms) {
    if (isMMS)
        throw std::invalid_argument("DischargeState cannot be used with MMS!");

    // Inner cells update
    auto& domain = owner_->domain_;
    auto& params = owner_->params_;

    unsigned int N = domain.ncells;
    double dx = domain.dx;
    double af = params.alpha_f;
    double as = params.alpha_s;

    auto& Tf = domain.fluid_temp;
    auto& newTf = domain.tmp_fluid_temp;

    auto& Ts = domain.solid_temp;
    auto& newTs = domain.tmp_solid_temp;

    for (unsigned int i = 1; i < N-1; i++) {
        // Fluid update
        newTf[i] = Tf[i] - u_f_*dt/dx*(Tf[i+1] - Tf[i])
                         + af*dt/(dx*dx)*(Tf[i+1] - 2*Tf[i] + Tf[i-1]);
        // Solid update
        newTs[i] = Ts[i] + as*dt/(dx*dx)*(Ts[i+1] - 2*Ts[i] + Ts[i-1]);

        // Coupling term
        std::tie(newTf[i], newTs[i]) = 
            eval_coupling_(dt, newTf[i], newTs[i]);
        /* Same as
         * auto c = eval_coupling_(dt, newTf[i], newTs[i]);
         * newTf[i] = c.first;
         * newTs[i] = c.second;
        */
    }

    // Boundary cells update
    doBoundaries_(dt, isMMS, mms);

    // Update temperatures to new values caculated
    Tf.swap(newTf);
    Ts.swap(newTs);
}

void DischargeState::doBoundaries_(double dt, bool isMMS, ManufacturedSolution* mms) {
    auto& domain = owner_->domain_;
    auto& params = owner_->params_;

    unsigned int N = domain.ncells;
    double dx = domain.dx;
    double af = params.alpha_f;
    double as = params.alpha_s;

    auto& Tf = domain.fluid_temp;
    auto& newTf = domain.tmp_fluid_temp;

    auto& Ts = domain.solid_temp;
    auto& newTs = domain.tmp_solid_temp;

    // Value at right boundary
    double T_bc = (*right_bc_)(0, cur_time_);

    newTf[0] = Tf[0] - u_f_*dt/dx*(Tf[1] - Tf[0])
                   + af*dt/(dx*dx)*(Tf[1] - Tf[0]);
    newTf[N-1] = Tf[N-1] - u_f_*dt/dx*(T_bc - Tf[N-1])
                     + af*dt/(dx*dx)*(Tf[N-2] - Tf[N-1]);

    newTs[0]   = Ts[0]   + as*dt/(dx*dx)*(Ts[1]   - Ts[0]);
    newTs[N-1] = Ts[N-1] + as*dt/(dx*dx)*(Ts[N-2] - Ts[N-1]);

    // Coupling term
    std::tie(newTf[0], newTs[0]) = 
        eval_coupling_(dt, newTf[0], newTs[0]);
    std::tie(newTf[N-1], newTs[N-1]) = 
        eval_coupling_(dt, newTf[N-1], newTs[N-1]);
}

