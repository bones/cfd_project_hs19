// SimulationState.cpp

#include "SimulationState.hpp"
#include "ThermoclineStorage.hpp"

unsigned int SimulationState::skip(unsigned int nsteps) {
    //std::cout << "SimulationState::skip(" << dt << ")\n";
    double dt = dt_;
    for (unsigned int i = 0; i < nsteps; i++) {
        double new_time = cur_time_ + dt;

        if (new_time >= duration_) {
            owner_->progress_state();
            return ++i;
        }

        cur_time_ = new_time;
    }
    return nsteps;
}

std::pair<double,double> 
    SimulationState::eval_coupling_(double dt, double Tf, double Ts)
{
    double hvf = owner_->params_.hvf;
    double hvs = owner_->params_.hvs;

    /* Linear system (7):
     * | a  b | | Tfn |   | Tf |
     * |      | |     | = |    |
     * | c  d | | Tsn |   | Ts |
     */
    double a, b, c, d;
    a = 1 + hvf * dt;   b =   - hvf * dt;
    c =   - hvs * dt;   d = 1 + hvs * dt;

    double Tfn = (Tf - b/d*Ts) / (a - b*c/d);
    double Tsn = (Ts - c*Tfn) / d;

    return {Tfn, Tsn};
}

void SimulationState::step_(double dt, bool isMMS, ManufacturedSolution* mms) {
    //std::cout << "SimulationState::step_("+std::to_string(dt)+")\n";
    
    // Inner cells update
    auto& domain = owner_->domain_;
    auto& params = owner_->params_;

    unsigned int N = domain.ncells;
    double dx = domain.dx;
    double af = params.alpha_f;
    double as = params.alpha_s;

    auto& Tf = domain.fluid_temp;
    auto& newTf = domain.tmp_fluid_temp;

    auto& Ts = domain.solid_temp;
    auto& newTs = domain.tmp_solid_temp;

    for (unsigned int i = 1; i < N-1; i++) {
        // Fluid update
        newTf[i] = Tf[i] - u_f_*dt/dx*(Tf[i] - Tf[i-1])
                         + af*dt/(dx*dx)*(Tf[i+1] - 2*Tf[i] + Tf[i-1]);
        // Solid update
        newTs[i] = Ts[i] + as*dt/(dx*dx)*(Ts[i+1] - 2*Ts[i] + Ts[i-1]);

        // Source terms if MMS available
        if (isMMS) {
            newTf[i] += dt/dx*mms->evalSourceTerm(
                    i*dx, (i+1)*dx, cur_time_
                );
            newTs[i] += dt/dx*mms->evalSourceTerm(
                    i*dx, (i+1)*dx, cur_time_, false
                );
        }

        // Coupling term
        std::tie(newTf[i], newTs[i]) = 
            eval_coupling_(dt, newTf[i], newTs[i]);
        /* Same as
         * auto c = eval_coupling_(dt, newTf[i], newTs[i]);
         * newTf[i] = c.first;
         * newTs[i] = c.second;
        */
    }

    // Boundary cells update
    doBoundaries_(dt, isMMS, mms);

    // Update temperatures to new values caculated
    Tf.swap(newTf);
    Ts.swap(newTs);
}

unsigned int SimulationState::step(unsigned int nsteps, bool steady_state, bool isMMS, ManufacturedSolution* mms) {
    double dt = dt_;
    //std::cout << "State: " << getStateType() << std::endl;
    //std::cout << "SimulationState::step(" << dt << ", " << nsteps << ")\n";
    //std::cout << "Duration: " << duration_ << std::endl;
    //std::cout << "cur time: " << cur_time_ << std::endl;
    //std::cout << "nsteps: " << getNTimeSteps() << std::endl;

    if (trackEnergy_) {
        double Td = 293; // TODO use config
        initial_stored_energy_ = owner_->domain_.computeThermalEnergy(Td);
        //std::cout << "Stored energy: " << initial_stored_energy_ << std::endl;

        Tinlet_.clear();
        Toutlet_.clear();
        timesteps_.clear();
    }

    unsigned int j = 0;
    for ( ; j < nsteps; j++) {
        // Advance time
        if (!steady_state) {
            double new_time = cur_time_ + dt;

            if (new_time <= duration_)
                cur_time_ = new_time;
            else
                break;
        }
        logTemps_(dt);
        step_(dt, isMMS, mms);
    }

    // Do last time step
    dt = duration_ - cur_time_;
    if (j < nsteps && dt > 0) {
        logTemps_(dt);
        step_(dt, isMMS, mms);
        j++;
        owner_->progress_state();
    }

    return j;
}

void SimulationState::logTemps_(double dt) {
    const auto& d = owner_->domain_;
    Tinlet_.push_back(d.fluid_temp[0]);
    Toutlet_.push_back(d.fluid_temp[d.ncells-1]);
    timesteps_.push_back(dt);
}

unsigned int SimulationState::getNTimeSteps() const {
    return std::ceil(duration_ / dt_);
}

void SimulationState::reset() {
    cur_time_ = 0;
}

std::pair<double, double> SimulationState::getExergyFlux() const {
    double Xi_in = 0;
    double Xi_out = 0;

    double mdot = owner_->params_.mdotf;
    double Cpf = owner_->params_.Cpf;
    double T0 = owner_->params_.Tref;

    const auto& dt = timesteps_;
    const auto& Tin = Tinlet_;
    const auto& Tout = Toutlet_;

    auto f = [&](double T) { return mdot*Cpf*(T - T0 - T0*std::log(T/T0)); };

    // Time integration with trapezoid quadrature
    for (unsigned int i = 0; i < timesteps_.size()-1; i++) {
        Xi_in  += .5 * dt[i] * (f(Tin [i]) + f(Tin [i+1]));
        Xi_out += .5 * dt[i] * (f(Tout[i]) + f(Tout[i+1]));
    }

    return std::pair<double, double>(Xi_in, Xi_out);
}

std::pair<double,double> SimulationState::getTemperatureChange() const {
    double dTl = Tinlet_[Tinlet_.size()-1] - Tinlet_[0];
    double dTr = Toutlet_[Toutlet_.size()-1] - Toutlet_[0];
    return std::pair<double, double>(dTl, dTr);
}

