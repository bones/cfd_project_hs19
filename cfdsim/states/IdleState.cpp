// IdleState.cpp

#include "IdleState.hpp"
#include "ThermoclineStorage.hpp"

void IdleState::doBoundaries_(double dt, bool isMMS, ManufacturedSolution* mms) {
    if (isMMS)
        throw std::invalid_argument("IdleState cannot be used with MMS!");

    auto& domain = owner_->domain_;
    auto& params = owner_->params_;

    unsigned int N = domain.ncells;
    double dx = domain.dx;
    double af = params.alpha_f;
    double as = params.alpha_s;

    auto& Tf = domain.fluid_temp;
    auto& newTf = domain.tmp_fluid_temp;

    auto& Ts = domain.solid_temp;
    auto& newTs = domain.tmp_solid_temp;

    /*
    // Value at left boundary
    double T_bc = (*left_bc_)(0, cur_time_);

    newTf[0] = Tf[0] - u_f_*dt/dx*(Tf[0] - T_bc)
                   + af*dt/(dx*dx)*(Tf[1] - Tf[0]);
    newTf[N-1] = Tf[N-1] - u_f_*dt/dx*(Tf[N-1] - Tf[N-2])
                     + af*dt/(dx*dx)*(Tf[N-2] - Tf[N-1]);
    */
    newTf[0] = Tf[0] + af*dt/(dx*dx)*(Tf[1] - Tf[0]);
    newTf[N-1] = Tf[N-1] + af*dt/(dx*dx)*(Tf[N-2] - Tf[N-1]);

    newTs[0]   = Ts[0]   + as*dt/(dx*dx)*(Ts[1]   - Ts[0]);
    newTs[N-1] = Ts[N-1] + as*dt/(dx*dx)*(Ts[N-2] - Ts[N-1]);

    // Coupling term
    std::tie(newTf[0], newTs[0]) = 
        eval_coupling_(dt, newTf[0], newTs[0]);
    std::tie(newTf[N-1], newTs[N-1]) = 
        eval_coupling_(dt, newTf[N-1], newTs[N-1]);
}

