// BoundaryConditions.hpp

#ifndef BOUNDARYCONDITIONS_H
#define BOUNDARYCONDITIONS_H

/**
 * Abstract base class for boundary conditions.
 */
class BoundaryCondition {
public:
    virtual double operator()(double x, double t) = 0;

    virtual ~BoundaryCondition() {
    
    }

    virtual BoundaryCondition* clone() const = 0;
};

/**
 * Helper that implements the clone() method on derived classes.
 * Ref: https://stackoverflow.com/questions/58708080/copy-constructing-data-members-of-abstract-type
 */
template <class Deriv>
class BCHelper : public BoundaryCondition {
public:
    virtual BoundaryCondition* clone() const {
        // Defer to copy-constructor
        return new Deriv(static_cast<const Deriv&>(*this));
    }
};

/**
 * Implementation of BoundaryCondition for constant value BCs.
 */
class ConstantBC : public BCHelper<ConstantBC> {
    double value_;
public:
    ConstantBC(double v)
        : value_(v)
    { }

    ConstantBC(const ConstantBC& other) {
        value_ = other.value_;
    }

    ~ConstantBC() {

    }

// pragmas to suppress warning: unused parameter t
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    double operator()(double x, double t) {
#pragma GCC diagnostic pop
        return value_;
    }
};

#endif
