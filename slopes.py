# slopes.py

import sys
import matplotlib.pyplot as plt
from numpy import log
import numpy as np

if len(sys.argv) < 2:
    print("Error: too few arguments!")
    print("Usage: ./plot.py input_data_filename")
    exit()

# Read command line arguments
datafile = sys.argv[1]

plottitle = ''
ylabel = ''
#if len(sys.argv) >= 4:
#    plottitle = sys.argv[3]
#    if len(sys.argv) >= 5:
#        ylabel = sys.argv[4]

# Read input data file
labels = []
data = []

with open(datafile) as f:
    content = f.read().splitlines()
    # First row is assumed to contain labels for each column
    labels = [l.strip() for l in content[0].split('\t')]

    # Use the number of labels to determine the number of data columns
    data = [[] for _ in range(len(labels))]
    for line in content[1:]:
        i = 0
        for num in line.split():
            data[i].append(float(num))
            i += 1

for j in range(1, len(labels)):
    print(labels[j] + " slopes (log-log)")
    s = []
    for i in range(len(data[0])-1):
        s.append((log(data[j][i+1]) - log(data[j][i])) / (log(data[0][i+1]) - log(data[0][i])))
        print(s[-1])
    print("Rounded median: " + str(np.round(np.median(s), 4)))
    print()

#if ylabel == '' and len(labels) == 2:
#    ylabel = labels[1]

# Initialize plot
#fig, ax = plt.subplots()

# Plot each data set, using first column as x-axis data
#for i in range(1, len(labels)):
#    ax.plot(data[0], data[i], label=labels[i], marker="o")

# Plot's aesthetics
#ax.set_title(plottitle)
#ax.set_xscale("log")
#ax.set_yscale("log")
#ax.set_xlabel(labels[0])
#ax.set_ylabel(ylabel)
#ax.legend()
#
## Save figure
#fig.savefig(outfile)
#plt.close(fig)

