// pt6_storagedesign.cpp

#include <iostream>
#include <fstream>
#include "ThermoclineStorage.hpp"
#include "cfdsim/ManufacturedSolutions.hpp"
#include "BoundaryConditions.hpp"

int main() {
    std::string outdir = "out/pt6/";

    double Tf_in_left = 873;
    double Tf_in_right = 293;
    double V = 300; // m^2

    std::vector<double> diams = {4, 5, 6, 7, 8};

    const double pi = std::atan(1) * 4;

    // Run a simulation for each diameter
    for (double diam : diams) {

        double height = 4*V / (pi * diam*diam);

        // Read parameters
        SimConfig cfg = SimConfig("pt6_domain.in", "pt6_parameters.in");

        // Set up config
        cfg.domain_diam = diam;
        cfg.domain_len = height;
        cfg.recalculate();

        // Log parameters
        std::cout << "##### Diameter: " << diam << "m #####" << std::endl;
        std::cout << std::endl;

        // Boundary conditions
        ConstantBC bcl(Tf_in_left);
        ConstantBC bcr(Tf_in_right);
        cfg.setBCs(&bcl, &bcr);

        // Calculate timestep according to CFL conditions
        cfg.applyCFLTimestep();
        cfg.checkCFL(true);

        // Initialize and run the simulation
        ThermoclineStorage sim(cfg);
        sim.simulate();

        // Output final temperatures to file
        std::string fname = outdir + "storagedesign_discharge_diam_" + std::to_string(diam) +".out";
        std::ofstream os;
        os.open(fname);
        sim.writeTemperatures(os, true);
        os.close();

        std::cout << "\n\n" << std::endl;
    }
}

