// ovs.cpp

#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <iomanip>
#include <omp.h>
#include "ThermoclineStorage.hpp"
#include "cfdsim/ManufacturedSolutions.hpp"
#include "BoundaryConditions.hpp"

struct JobResult {
    double dx;
    double l1Error_f;
    double lInfError_f;
    double l1Error_s;
    double lInfError_s;
    unsigned int nsteps;
    double runtime;
};

struct Job {
    unsigned int ref;
    double Pe;
    unsigned int n;
    SimConfig cfg;

    JobResult& result;
};

int main() {
    std::cout << "\n\nOrder Verification Study with Method of Manufactured Solutions\n";
    auto now = std::chrono::system_clock::now();
    auto now_c = std::chrono::system_clock::to_time_t(now);
    std::cout << "\nStarted: " 
              << std::put_time(std::localtime(&now_c), "%c")
              << "\n" << std::endl;

    std::string outdir = "out/";
    std::string ovsoutdir = "out/ovs/";

    const double pi = std::atan(1) * 4;

    const std::vector<unsigned int> n_vals = {1, 2, 4};
    const std::vector<double>       Pe_vals = {1e-3, 1, 1e3};

    const unsigned int n_refinements = 10;

    // Construct list of config objects to facilitate parallelism
    std::vector<Job> jobs;

    // Multi-dim. vector of all configurations
    // Indexing: results[Pe][n][ref] = JobResult
    std::vector<std::vector<std::vector<JobResult>>> results(
            Pe_vals.size(), 
            std::vector<std::vector<JobResult>>(
                n_vals.size(), 
                std::vector<JobResult>(n_refinements)
            )
        );

    // Read parameters
    SimConfig cfg = SimConfig("ovs_domain.in", "ovs_parameters.in");

    // Log parameters
    std::cout << "MMS function: cos(kx)" << std::endl;
    std::cout << "Tolerance for steady-state convergence: " << cfg.tol << "\n";
    std::cout << "Refinement iterations: " << n_refinements << "\n";
    std::cout << "Peclet number values: ";
    for (auto Pe : Pe_vals)
        std::cout << Pe << "\t";
    std::cout << "\nn values: ";
    for (auto n : n_vals)
        std::cout << n << "\t";
    std::cout << std::endl;
    std::cout << "\nSetting up jobs...";

    // Set up configs for each simulation to run
    for (unsigned int ref = 0; ref < n_refinements; ref++, cfg.ncells *= 2) {
        for (unsigned int iPe = 0; iPe < Pe_vals.size(); iPe++) {
            double Pe = Pe_vals[iPe];
            cfg.applyPecletNumber(Pe);
            //cfg.checkCFL(true);
            
            for (unsigned int in = 0; in < n_vals.size(); in++) {
                unsigned int n = n_vals[in];

                double kf = 2*n*pi/cfg.domain_len;
                double ks = 2*(n+1)*pi/cfg.domain_len;
                CoskxMMS mms(
                        kf, ks, cfg.u_f_charging, cfg.alpha_f, cfg.alpha_s, 
                        cfg.hvf, cfg.hvs, cfg.domain_len
                    );
                cfg.setMMS(&mms);

                jobs.push_back({
                        ref,
                        Pe,
                        n,
                        SimConfig(cfg),
                        results[iPe][in][ref]
                });
            }
        }
    }

    std::cout << " Done." << std::endl;

    // Initialize and execute each simulation in parallel
//#pragma omp parallel for schedule(dynamic, 1)
    for (unsigned int i = 0; i < jobs.size(); i++) {
        Job& job = jobs[i];

//#pragma omp critical
        std::cout << "\n[TID=" << omp_get_thread_num() << "] Running job "
            << i+1 << " of " << jobs.size() 
            << "\n\tPe = " << job.Pe 
            << "\n\tn = " << job.n
            << "\n\tncells = " << job.cfg.ncells
            << std::endl;

        // Initialize and run the simulation
        ThermoclineStorage sim(job.cfg);
        //sim.printSummary();
        sim.simulate();

        double l1ef = sim.l1Error_f();
        double linfef = sim.lInfError_f();
        
        double l1es = sim.l1Error_s();
        double linfes = sim.lInfError_s();
        
        // Log results
        job.result = {
            job.cfg.domain_len/job.cfg.ncells,
            l1ef,
            linfef,
            l1es,
            linfes,
            sim.getStepCount(),
            sim.getRuntime()
        };

        // Output final temperatures to file
        std::string fname = outdir + "finaltemperature" 
            + "_Pe" + std::to_string(job.Pe)
            + "_n" + std::to_string(job.n)
            + "_ncells" + std::to_string(job.cfg.ncells)
            + ".out";
        std::ofstream os;
        os.open(fname);
        sim.writeTemperatures(os, true);
        os.close();
    }


    // Collate results and write to output files
    for (unsigned int iPe = 0; iPe < Pe_vals.size(); iPe++) {
        double Pe = Pe_vals[iPe];
        std::cout << Pe << std::endl;
        for (unsigned int in = 0; in < n_vals.size(); in++) {
            unsigned int n = n_vals[in];

            // Generate file name
            std::string fname = ovsoutdir + "ovs_Pe" + std::to_string(Pe)
                                + "_n" + std::to_string(n) + ".out";

            std::cout << "\t" << n << std::endl;
            std::cout << "\t\t";
            std::cout << fname << "\n\t\t";

            // Output stream to file
            std::ofstream os;
            os.open(fname);
            os  << Pe << "\n"
                << n << "\n";
            os  << "h\tL1 error (fluid)\tL-Inf error (fluid)"
                << "\tL1 error (solid)\tL-Inf error (solid)"
                << "\tnsteps\tRuntime [s]\n";

            for (unsigned int iref = 0; iref < n_refinements; iref++) {
                const JobResult& res = results[iPe][in][iref];
                std::cout << iref << " " << res.l1Error_f << "\n\t\t";
                os  << res.dx << "\t" 
                    << res.l1Error_f << "\t" 
                    << res.lInfError_f << "\t"
                    << res.l1Error_s << "\t" 
                    << res.lInfError_s << "\t"
                    << res.nsteps << "\t"
                    << res.runtime
                    << std::endl;
            }
            os.close();
            std::cout << std::endl;
        }
    }

    now = std::chrono::system_clock::now();
    now_c = std::chrono::system_clock::to_time_t(now);
    std::cout << "\n\nEnded: " 
              << std::put_time(std::localtime(&now_c), "%c")
              << "\n" << std::endl;
}

