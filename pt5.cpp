// pt5.cpp

#include <iostream>
#include <fstream>
#include "ThermoclineStorage.hpp"
#include "cfdsim/ManufacturedSolutions.hpp"
#include "BoundaryConditions.hpp"

int main() {
    std::string outdir = "out/pt5/";

    double Tf_in_left = 873;

    unsigned int n_refinements = 9;

    // Read parameters
    SimConfig cfg = SimConfig("pt5_domain.in", "pt5_parameters.in");

    // Log parameters
    std::cout << "ncells0: " << cfg.ncells << std::endl;
    std::cout << "n_refinements: " << n_refinements << std::endl;
    std::cout << "alpha_f: " << cfg.alpha_f << std::endl;
    std::cout << "alpha_s: " << cfg.alpha_s << std::endl;
    std::cout << std::endl;

    // Set up BCs
    ConstantBC bcl(Tf_in_left);
    cfg.setBCs(&bcl, nullptr);

    for (unsigned int ref = 0; ref < n_refinements; ref++, cfg.ncells *= 2) {
        std::cout << "ncells: " << cfg.ncells << std::endl;
        cfg.applyCFLTimestep();
        cfg.checkCFL(true);

        // Initialize and run the simulation
        ThermoclineStorage sim(cfg);
        sim.simulate();

        std::cout << "Done." << std::endl;

        // Output final temperatures to file
        std::string fname = outdir + "pt5_" + std::to_string(cfg.ncells) + ".out";
        std::ofstream os;
        os.open(fname);
        sim.writeTemperatures(os, false);
        os.close();
    }

    return 0;
}

